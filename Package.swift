// swift-tools-version:5.3

import PackageDescription

let package = Package(
    name: "HellocarePlatformSDK",
    platforms: [
        .iOS(.v12)
    ],
    products: [
        .library(
            name: "HellocarePlatformSDK",
            targets: ["HellocarePlatformSDK", "TwilioVideo"]),
    ],
    targets: [
        .binaryTarget(
            name: "HellocarePlatformSDK",
            url: "https://bitbucket.org/hellocare/plateforme-sdk-ios/src/master/release/download/2.3.2/HellocarePlatformSDK.xcframework.zip",
            checksum: "dcd3152458798ea035b52bd2d01ed0641ebeea8d6d6cc5eacf5d3508c0c9f9d4"
        ),
        .binaryTarget(
            name: "TwilioVideo",
            url: "https://github.com/twilio/twilio-video-ios/releases/download/5.0.0/TwilioVideo.xcframework.zip",
            checksum: "caf63ceb51e6ef56d881da3e01713fedfd566c9371842f249c49fcc186a265c6"
        )
    ]
)
